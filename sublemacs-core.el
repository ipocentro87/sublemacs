(setq dired-ranger-cut nil)

;; Go to the upper level magit status
(defun sublemacs-core/magit-status-up ()
  (interactive)
  (magit-status (magit-toplevel "../")))

;; Tell the user to close the minibuffer before calling
;; persp-prev/next, as it causes windows misplacements
(defun sublemacs/persp-prev ()
  (interactive)
  (if (active-minibuffer-window)
      (message "Close the minibuffer before calling persp-prev!")
      (persp-prev)))

(defun sublemacs/persp-next ()
  (interactive)
  (if (active-minibuffer-window)
      (message "Close the minibuffer before calling persp-next!")
      (persp-next)))

(setq lineno-format "%4s%s%s")

(defun helm-occur-transformer (candidates source)
  "Return CANDIDATES prefixed with line number."
  (cl-loop with buf = (helm-get-attr 'buffer-name source)
           for c in candidates
           for disp-linum = (when (string-match helm-occur--search-buffer-regexp c)
                              (let ((linum (match-string 1 c))
                                    (disp (match-string 2 c)))
                                (list
                                 linum
                                 (format lineno-format
                                         (propertize
                                          linum 'face 'helm-grep-lineno
                                          'help-echo (buffer-file-name
                                                      (get-buffer buf)))
                                         (propertize ":" 'face 'helm-grep-lineno)
                                         disp))))
           for linum = (car disp-linum)
           for disp = (cadr disp-linum)
           when (and disp (not (string= disp "")))
           collect (cons disp (string-to-number linum))))

;;;###autoload
(defun sublemacs-core/helm-occur (&key query)
  "Preconfigured helm for searching lines matching pattern in `current-buffer'.

When `helm-source-occur' is member of
`helm-sources-using-default-as-input' which is the default,
symbol at point is searched at startup.

When a region is marked search only in this region by narrowing.

To search in multiples buffers start from one of the commands listing
buffers (i.e. a helm command using `helm-source-buffers-list' like
`helm-mini') and use the multi occur buffers action.

This is the helm implementation that collect lines matching pattern
like vanilla Emacs `occur' but have nothing to do with it, the search
engine beeing completely different and also much faster."
  (interactive)
  (setq helm-source-occur
        (car (helm-occur-build-sources (list (current-buffer)) "Helm occur")))
  (helm-set-local-variable 'helm-occur--buffer-list (list (current-buffer))
                           'helm-occur--buffer-tick
                           (list (buffer-chars-modified-tick (current-buffer))))
  (helm-set-attr 'header-name (lambda (_name)
                                (format "Search at %s"
                                        (buffer-name helm-current-buffer)))
                 helm-source-occur)
  (when helm-occur-keep-closest-position
    (setq helm-occur--initial-pos (line-number-at-pos))
    (add-hook 'helm-after-update-hook 'helm-occur--select-closest-candidate))
  (save-restriction
    (let ((helm-sources-using-default-as-input
           (unless (> (buffer-size) 2000000)
             helm-sources-using-default-as-input))
          def pos)
      (when (and (use-region-p) (> (count-lines (region-beginning) (region-end)) 1))
        ;; When user mark defun with `mark-defun' with intention of
        ;; using helm-occur on this region, it is relevant to use the
        ;; thing-at-point located at previous position which have been
        ;; pushed to `mark-ring'.
        (setq def (save-excursion
                    (goto-char (setq pos (car mark-ring)))
                    (helm-aif (thing-at-point 'symbol) (regexp-quote it))))
        (narrow-to-region (region-beginning) (region-end)))
      (unwind-protect
          (helm :sources 'helm-source-occur
                :input  query
                :buffer "*helm occur*"
                :history 'helm-grep-history
                :default (or def (helm-aif (thing-at-point 'symbol)
                                     (regexp-quote it)))
                :preselect (and (memq 'helm-source-occur
                                      helm-sources-using-default-as-input)
                                (format "^%d:" (line-number-at-pos
                                                (or pos (point)))))
                :truncate-lines helm-occur-truncate-lines)
        (deactivate-mark t)
        (remove-hook 'helm-after-update-hook 'helm-occur--select-closest-candidate)))))

(defun sublemacs-core/swoop ()
  (interactive)
  (if (and (region-active-p) (eq (count-lines (region-beginning) (region-end)) 1))
      (progn
        (setq r (buffer-substring-no-properties (mark) (point)))
        (if (not (equal (car helm-grep-history) r))
            (push r helm-grep-history))))
  (setq query (car helm-grep-history))
  (let  ((lineno-format (concat "%" (number-to-string (string-width (number-to-string (count-lines (point-min) (point-max))))) "s%s%s")))
      (sublemacs-core/helm-occur :query (if query query ""))
  )
  (deactivate-mark))

(defun sublemacs-core/find-git-superproject ()
  (interactive)
  (setq path (file-name-as-directory default-directory))
  (setq ret  nil)
  (while (magit-toplevel path)
    (setq ret (magit-toplevel path))
    (setq path (concat path "../")))
  (expand-file-name ret))

;; Find higher untracked folder
(defun is_untracked (path) (string= (shell-command-to-string (concat "git ls-files " path " --directory")) ""))
(defun sublemacs-core/find-git-untracked ()
  (interactive)
  (setq path (file-name-as-directory default-directory))
  (setq path-old path)
  (while (is_untracked path)
    (progn
      (setq path-old path)
      (setq path (concat path "../"))))
  (expand-file-name path-old))

(defun sublemacs-core/git-grep (arg)
  (interactive "P")
  (if (region-active-p)
      (progn
        (setq r (buffer-substring-no-properties (mark) (point)))
        (if (not (equal (car helm-grep-history) r))
            (push r helm-grep-history))))
  (setq query (car helm-grep-history))
  (helm-grep-git-1 (sublemacs-core/find-git-superproject) arg :input (if query query ""))
  (deactivate-mark))

(defun sublemacs-core/git-grep-this-repo (arg)
  (interactive "P")
  (if (region-active-p)
      (progn
        (setq r (buffer-substring-no-properties (mark) (point)))
        (if (not (equal (car helm-grep-history) r))
            (push r helm-grep-history))))
  (setq query (car helm-grep-history))
  (helm-grep-git-1 (magit-toplevel (file-name-as-directory default-directory)) arg :input (if query query ""))
  (deactivate-mark))

(defun sublemacs-core/grep-this-directory (arg)
  (interactive "P")
  (if (region-active-p)
      (progn
        (setq r (buffer-substring-no-properties (mark) (point)))
        (if (not (equal (car helm-grep-history) r))
            (push r helm-grep-history))))
  (setq query (car helm-grep-history))
  (helm-do-grep-ag nil)
  (deactivate-mark))

(defun sublemacs-core/projectile-find-file (&optional arg)
  (interactive "P")
  (let ((default-directory (sublemacs-core/find-git-superproject))) (projectile-find-file)))

(defun sublemacs-core/projectile-find-file-this-repo (&optional arg)
  (interactive "P")
  (let ((projectile-git-command "git ls-files -z --exclude-standard")) (helm-projectile-find-file arg)))

(defun sublemacs-core/ffap (arg)
  (interactive "P")
  (let ((helm-execute-action-at-once-if-one t))
       (helm-find-files arg)))

(defun sublemacs-core/narrow-or-widen ()
  (interactive)
  (if (buffer-narrowed-p)
      (widen)
    (if (region-active-p)
        (narrow-to-region (region-beginning) (region-end)))))

(defun sublemacs-core/dired ()
  (interactive)
  (dired default-directory))

(defun sublemacs-core/save-buffer (&optional arg)
  (interactive "p")
  (delete-trailing-whitespace)
  (save-buffer arg))

(defun sublemacs-core/toggle-maximize-buffer ()
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows)))
  (redraw-display))

(defun sublemacs-core/split-window-vertically ()
  (interactive)
  (split-window-vertically)
  (other-window 1))

(defun sublemacs-core/split-window-horizontally ()
  (interactive)
  (split-window-horizontally)
  (other-window 1))

(defun sublemacs-core/next-hunk ()
  "Move to next diff hunk"
  (interactive)
  (if (region-active-p) (deactivate-mark))
  (diff-hl-next-hunk nil)
  (recenter))

(defun sublemacs-core/previous-hunk ()
  "Move to previous diff hunk"
  (interactive)
  (if (region-active-p) (deactivate-mark))
  (diff-hl-previous-hunk)
  (recenter))

(defun sublemacs-core/keyboard-quit ()
  (interactive)
  (mc/keyboard-quit)
  ; (setq sublemacs-mark-mode 0)
  (cond ((eq last-command 'mode-exited) nil)
        ((region-active-p)
         (deactivate-mark))
        ((> (minibuffer-depth) 0)
         (abort-recursive-edit))
        (current-prefix-arg
         nil)
        ((> (recursion-depth) 0)
         (exit-recursive-edit))
        (buffer-quit-function
         (funcall buffer-quit-function))
        ((string-match "^ \\*" (buffer-name (current-buffer)))
         (bury-buffer)))
  ;;
  (keyboard-quit)
  )

(defun sublemacs-core/shell (&optional buffer)
  (interactive
   (list
    (and current-prefix-arg
         (prog1
             (read-buffer "Shell buffer: "
                          ;; If the current buffer is an inactive
                          ;; shell buffer, use it as the default.
                          (if (and (eq major-mode 'shell-mode)
                                   (null (get-buffer-process (current-buffer))))
                              (buffer-name)
                            (generate-new-buffer-name "*shell*")))
           (if (file-remote-p default-directory)
               ;; It must be possible to declare a local default-directory.
               ;; FIXME: This can't be right: it changes the default-directory
               ;; of the current-buffer rather than of the *shell* buffer.
               (setq default-directory
                     (expand-file-name
                      (read-directory-name
                       "Default directory: " default-directory default-directory
                       t nil))))))
    ))
  (let ((process-environment (cons "TERM=xterm" process-environment)))
    (shell buffer))
  )

(defun sublemacs-core/shell-same-window-advice (orig-fn &optional buffer)
  "Advice to make `shell' reuse the current window.
Intended as :around advice."
  (let* ((buffer-regexp
          (regexp-quote
           (cond ((bufferp buffer)  (buffer-name buffer))
                 ((stringp buffer)  buffer)
                 (:else             "*shell*"))))
         (display-buffer-alist
          (cons `(,buffer-regexp display-buffer-same-window)
                display-buffer-alist)))
    (funcall orig-fn buffer)))

(defun sublemacs-core/select-whole-line ()
  (interactive "^p")
  (move-beginning-of-line 1)
  (when (not (region-active-p))  ;; if the region is not active...
    (push-mark (point) t t))     ;; ... set the mark and activate it
  (move-end-of-line 1))

(defun sublemacs-core/magit-submodule-update-recursive ()
  (interactive)
  (magit-run-git-async "submodule" "update" "--init" "--recursive"))

(defun sublemacs-core/set-system-clipboard (clipboard)
    "Set system clipboard"
    (interactive)
    (if (eq system-type 'darwin)
        (with-temp-buffer
            (insert clipboard)
            (call-process-region (point-min) (point-max) "pbcopy"))
        (with-temp-buffer
            (insert clipboard)
            (clipboard-kill-ring-save (point-min) (point-max)))))

(defun sublemacs-core/get-system-clipboard ()
    "Get system clipboard"
    (interactive)
        (with-output-to-string
            (with-current-buffer standard-output
              (if (eq system-type 'darwin)
                (call-process "pbpaste" nil t nil "-Prefer" "txt")
                (clipboard-yank)))))

(setq sublemacs-core-mc-regions '())
(setq sublemacs-core-mc-idx 0)
(setq sublemacs-core-clipboard "")

(defun sublemacs-core/right-char (&optional n)
    (interactive)
    (setq right-border (max (or (mark) (point)) (point)))
    (if (region-active-p)
        (progn
          (deactivate-mark)
          (goto-char right-border))
      (right-char n)))

(defun sublemacs-core/left-char (&optional n)
    ""
    (interactive)
    (setq left-border (min (or (mark) (point)) (point)))
    (if (region-active-p)
        (progn
          (deactivate-mark)
          (goto-char left-border))
      (left-char n)))

(defun sublemacs-core/shift-right-char ()
    ""
    (interactive)
    (when (not (region-active-p))  ;; if the region is not active...
        (push-mark (point) t t))   ;; ... set the mark and activate it
    (right-char))

(defun sublemacs-core/shift-left-char ()
    ""
    (interactive)
    (when (not (region-active-p))  ;; if the region is not active...
        (push-mark (point) t t))   ;; ... set the mark and activate it
    (left-char))

(defun sublemacs-core/mc-reset-regions ()
    (interactive)
    (setq sublemacs-core-mc-regions '())
    (setq sublemacs-core-clipboard "")
)

(defun sublemacs-core/mc-load-regions ()
    (interactive)
    (if cua--rectangle
        (setq region_str (mapconcat (function (lambda (row) (concat row "\n"))) (cua--extract-rectangle) ""))
        (setq region_str (buffer-substring-no-properties (region-beginning) (region-end))))
    (setq region `(,(region-beginning) ,region_str))
    (setq sublemacs-core-mc-regions (add-to-list 'sublemacs-core-mc-regions region t)))

(defun sublemacs-core/mc-store-regions ()
    (interactive)
    (setq region (nth sublemacs-core-mc-idx sublemacs-core-mc-regions))
    (setq sublemacs-core-mc-idx (+ sublemacs-core-mc-idx 1))
    (insert (nth 1 region)))

(defun sublemacs-core/mc-delete-regions ()
    (interactive)
    (if (region-active-p)
        (if cua--rectangle
            (cua--delete-rectangle)
            (delete-region (region-beginning) (region-end)))))

(defun sublemacs-core/mc-store-clipboard ()
    (interactive)
    (insert (sublemacs-core/get-system-clipboard)))

(defun sublemacs-core/copy(arg)
    "Copy region to OS X system pasteboard."
    (interactive "P")
    (sublemacs-core/mc-reset-regions)
    (mc/execute-command-for-all-cursors 'sublemacs-core/mc-load-regions)
    (setq sublemacs-core-mc-regions
        (sort sublemacs-core-mc-regions #'(lambda (p1 p2) (< (nth 0 p1) (nth 0 p2)))))
    (setq sublemacs-core-clipboard "")
    (cl-loop for r in sublemacs-core-mc-regions
        do (if (equal sublemacs-core-clipboard "")
                    (setq sublemacs-core-clipboard (nth 1 r))
                    (setq sublemacs-core-clipboard
                        (concat sublemacs-core-clipboard "\n" (nth 1 r)))))
    (sublemacs-core/set-system-clipboard sublemacs-core-clipboard)
)

(defun sublemacs-core/paste(arg)
    "Copy region to OS X system pasteboard."
    (interactive "P")
    (setq sublemacs-core-mc-idx 0)
    (setq num-cursors (or (mc/num-cursors) 1))
    (mc/execute-command-for-all-cursors 'sublemacs-core/mc-delete-regions)
    (if (and (= num-cursors (length sublemacs-core-mc-regions))
             (> num-cursors 1)
             (equal sublemacs-core-clipboard (sublemacs-core/get-system-clipboard)))
        (mc/for-each-cursor-ordered
             (mc/execute-command-for-fake-cursor 'sublemacs-core/mc-store-regions cursor))
        (progn
            (mc/execute-command-for-all-cursors 'sublemacs-core/mc-store-clipboard)
            )))

(defun sublemacs-core/mc-execute (arg)
  (interactive "P")
  (if (eq (mc/num-cursors) 1)
     (sublemacs-core/execute)
      (mc/for-each-cursor-ordered
       (mc/execute-command-for-fake-cursor 'sublemacs-core/execute cursor))))

(defun sublemacs-core/execute ()
  (interactive)
  (if (region-active-p)
      (progn
        (setq region (buffer-substring (mark) (point)))
        (delete-region (region-beginning) (region-end))
        (insert (shell-command-to-string (format "/usr/bin/python3 -c 'print(%s, end=str())'" region))))))

(defun sublemacs-core/cut(arg)
    "Cut region and put on OS X system pasteboard."
    (interactive "P")
    (sublemacs-core/copy arg)
    (mc/execute-command-for-all-cursors 'sublemacs-core/mc-delete-regions))

(defun sublemacs-core/dired-copy(arg)
    "Copy region to OS X system pasteboard."
    (interactive "P")
    (if (region-active-p)
        (message "Region is active, you must deactivate it before doing dired-copy")
      (progn
        (dired-ranger-copy arg)
        (setq dired-ranger-cut nil))))

(defun sublemacs-core/dired-paste(arg)
    "Copy region to OS X system pasteboard."
    (interactive "P")
    (if (region-active-p)
        (message "Region is active, you must deactivate it before doing dired-paste")
      (progn
        (if dired-ranger-cut
            (dired-ranger-move arg)
          (dired-ranger-paste arg)))))

(defun sublemacs-core/dired-cut(arg)
    "Cut region and put on OS X system pasteboard."
    (interactive "P")
    (if (region-active-p)
        (message "Region is active, you must deactivate it before doing dired-cut")
      (progn
        (dired-ranger-copy arg)
        (setq dired-ranger-cut t))))

(defun sublemacs-core/advice-bypass-confirmation (function &rest args)
  "Call FUNCTION with ARGS, bypassing all `y-or-n-p' prompts."
  (let ((y-or-n-p (lambda (prompt) t)))
    (apply function args)))

(advice-add 'sublemacs-core/dired-paste :around #'sublemacs-core/advice-bypass-confirmation)

(defun sublemacs-core/mc-cursor-up (&optional arg try-vscroll)
  (interactive "^p\np")
  (mc/create-fake-cursor-at-point)
  (previous-line)
  (multiple-cursors-mode 1))

(defun sublemacs-core/mc-cursor-down (&optional arg try-vscroll)
  (interactive "^p\np")
  (mc/create-fake-cursor-at-point)
  (next-line)
  (multiple-cursors-mode 1))

(setq sublemacs-core-shift-translated nil)
(setq sublemacs-core-region-active nil)
(setq sublemacs-core-left-border nil)
(setq sublemacs-core-right-border nil)

(defun sublemacs-core/tab-name-function ()
  (setq ret "")
  (cl-loop for persp-name in (persp-all-names)
           do (if (eq persp-name (persp-current-name))
                  (setq ret (concat ret " [" persp-name "]"))
                (setq ret (concat ret "  " persp-name " ")))
           )
  ret
)

;; Thanks to https://www.reddit.com/r/emacs/comments/cwnd57/comment/eydfyr9/?utm_source=share&utm_medium=web2x&context=3
(defun sublemacs-core/get-current-buffer-filename ()
  (interactive)
  (let ((buf (if (active-minibuffer-window)
                 (window-buffer (minibuffer-selected-window))
               (current-buffer))))
    ;; use full file name if there one, otherwise just buffer name.
    (or (buffer-file-name buf) (buffer-name buf))))

(defun sublemacs-core/copy-current-buffer-filename ()
  (interactive)
  (sublemacs-core/set-system-clipboard (sublemacs-core/get-current-buffer-filename)))

;; From https://emacs.stackexchange.com/questions/24459/revert-all-open-buffers-and-ignore-errors
(defun sublemacs-core/revert-all-file-buffers ()
  "Refresh all open file buffers without confirmation.
Buffers in modified (not yet saved) state in emacs will not be reverted. They
will be reverted though if they were modified outside emacs.
Buffers visiting files which do not exist any more or are no longer readable
will be killed."
  (interactive)
  (dolist (buf (buffer-list))
    (let ((filename (buffer-file-name buf)))
      ;; Revert only buffers containing files, which are not modified;
      ;; do not try to revert non-file buffers like *Messages*.
      (when (and filename
                 (not (buffer-modified-p buf)))
        (if (file-readable-p filename)
            ;; If the file exists and is readable, revert the buffer.
            (with-current-buffer buf
              (revert-buffer :ignore-auto :noconfirm :preserve-modes))
          ;; Otherwise, kill the buffer.
          (let (kill-buffer-query-functions) ; No query done when killing buffer
            (kill-buffer buf)
            (message "Killed non-existing/unreadable file buffer: %s" filename))))))
  (message "Finished reverting buffers containing unmodified files."))

(defun sublemacs-core/uniquify-region-lines (beg end)
  "Remove duplicate adjacent lines in region."
  (interactive "*r")
  (save-excursion
    (goto-char beg)
    (while (re-search-forward "^\\(.*\n\\)\\1+" end t)
      (replace-match "\\1"))))

(defun sublemacs-core/uniquify-buffer-lines ()
  "Remove duplicate adjacent lines in the current buffer."
  (interactive)
  (uniquify-region-lines (point-min) (point-max)))

;; Inspired by: https://emacs.stackexchange.com/questions/28502/magit-show-ignored-files
;; Override existing magit-insert-ignored-files
;; TODO: push to magit repo
(defun sublemacs-core/magit-insert-ignored-files ()
  (when-let ((files (magit-ignored-files)))
    (magit-insert-section (untracked)
      (magit-insert-heading "Ignored files:")
      (dolist (file files)
        (magit-insert-section (file file)
          (insert (propertize file 'font-lock-face 'magit-filename) ?\n)))
      (insert ?\n))))

(defun sublemacs-core/insert-into-list (list el n)
  "Insert into list LIST an element EL at index N.

If N is 0, EL is inserted before the first element.

The resulting list is returned.  As the list contents is mutated
in-place, the old list reference does not remain valid."
  (let* ((padded-list (cons nil list))
         (c (nthcdr n padded-list)))
    (setcdr c (cons el (cdr c)))
    (cdr padded-list)))

(defun sublemacs-core/find-next-unsafe-char (&optional coding-system)
  "Find the next character in the buffer that cannot be encoded by
coding-system. If coding-system is unspecified, default to the coding
system that would be used to save this buffer. With prefix argument,
prompt the user for a coding system."
  (interactive "Zcoding-system: ")
  (if (stringp coding-system) (setq coding-system (intern coding-system)))
  (if coding-system nil
    (setq coding-system
          (or save-buffer-coding-system buffer-file-coding-system)))
  (let ((found nil) (char nil) (csets nil) (safe nil))
    (setq safe (coding-system-get coding-system 'safe-chars))
    ;; some systems merely specify the charsets as ones they can encode:
    (setq csets (coding-system-get coding-system 'safe-charsets))
    (save-excursion
      ;;(message "zoom to <")
      (let ((end  (point-max))
            (here (point    ))
            (char  nil))
        (while (and (< here end) (not found))
          (setq char (char-after here))
          (if (or (eq safe t)
                  (< char ?\177)
                  (and safe  (aref safe char))
                  (and csets (memq (char-charset char) csets)))
              nil ;; safe char, noop
            (setq found (cons here char)))
          (setq here (1+ here))) ))
    (and found (goto-char (1+ (car found))))
    found))
