# Prerequisites

```bash
sudo apt-get install libsqlite3-dev silversearcher-ag ispell

# Go to this repo root dir
cd sublemacs

# Set emacs path
mkdir -p ~/.emacs.d
ln -s `realpath init.el` ~/.emacs.d/init.el

# Set kitty term conf (if emacs term is used)
mkdir -p ~/.config/kitty
ln -s `realpath kitty.conf` ~/.config/kitty/kitty.conf

# Set terminfo (xterm-emacs)
tic -x -o ~/.terminfo .terminfo
# In order to have proper colors on the terminal, TERM=xterm-emacs:
# Eg. TERM=xterm-emacs emacs -nw

# Install Hack Nerd Fonts, or edit custom.el to pick your preferred one
```
