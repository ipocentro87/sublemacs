;; -*- lexical-binding: t; -*-

;; Enable debug-on-error
(setq debug-on-error t)

;; -------------------------
;; -- Startup Performance --
;; -------------------------
;; Make startup faster by reducing the frequency of garbage collection
;; and then use a hook to measure Emacs startup time.
;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

;; -------------------
;; -- Load packages --
;; -------------------
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(setq package-archives
      '(("gnu"       . "https://elpa.gnu.org/packages/")
        ("gnu-devel" . "https://elpa.gnu.org/devel/")
        ("nongnu"    . "https://elpa.nongnu.org/nongnu/")
        ("melpa"     . "https://melpa.org/packages/")))

(setq package-selected-packages '(
                     emacsql-sqlite-module
                     emacsql-sqlite
                     sqlite
                     sqlite3
                     magit
                     forge
                     code-review

                     all-the-icons
                     company
                     company-posframe
                     native-complete
                     company-native-complete
                     diff-hl
                     doom-modeline
                     doom-themes
                     nerd-icons
                     drag-stuff
                     elf-mode
                     helm
                     helm-ag
                     projectile
                     ini-mode
                     markdown-mode
                     multiple-cursors
                     perspective
                     pdf-tools
                     undo-tree
                     use-package
                     verilog-mode
                     vlf
                     expand-region
                     wgrep
                     coterm
                     goto-chg
                     evil
                     proof-general
                     company-coq
                     dired-ranger
))

(when (< emacs-major-version 27)
  (package-initialize))

;; Install the missing packages
(dolist (package package-selected-packages)
  (unless (package-installed-p package)
    (progn
      ; fetch the list of packages available
      (unless package-archive-contents (package-refresh-contents))
      (package-install package))))

;; -------------------
;; -- Load function --
;; -------------------
(defun ld (path)
  (load-file
   (concat
    (file-name-directory
     (file-chase-links "~/.emacs.d/init.el"))
    path)))

(setq evil-default-state "emacs")

(load-library "view")

;; ------------------
;; -- Requirements --
;; ------------------
(require 'emacsql-sqlite-module)
(require 'emacsql-sqlite)
(require 'sqlite)
(require 'sqlite3)
(require 'magit)
(require 'forge)
(require 'code-review)
(require 'evil)

(require 'company)
(require 'company-posframe)
(require 'company-native-complete)
(require 'diff-hl)
(require 'doom-modeline)
(require 'nerd-icons)
(require 'drag-stuff)
(require 'elf-mode)
(require 'flyspell)
(require 'helm)
(require 'helm-ag)
(require 'helm-files)
(require 'multiple-cursors)
(require 'perspective)
(require 'profiler)
(require 'projectile)
(require 'tramp)
(require 'undo-tree)
(require 'verilog-mode)
(require 'vlf-setup)
(require 'expand-region)
(require 'wgrep)
(require 'dired-ranger)

(setq wgrep-auto-save-buffer t)
(setq wgrep-change-readonly-file t)

(ld "sublemacs-core.el")

;; ------------------
;; -- Emacs config --
;; ------------------
;; Make _ as part of the word
(modify-syntax-entry ?_ "w" (standard-syntax-table))

;; Avoid constant errors on Windows about the coding system by setting
;; the default to UTF-8.
(set-default-coding-systems 'utf-8)

;; Window divider
(setq window-divider-default-right-width 3)
(window-divider-mode 1)

;; Load company-coq when opening Coq files
(add-hook 'coq-mode-hook #'company-coq-mode)

;; make backup to a designated dir, mirroring the full path
(defun my-backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* (
        (backupRootDir "~/.emacs.d/backup/")
        (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath )) ; remove Windows driver letter in path, for example, “C:”
        (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") ))
        )
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath))

(setq make-backup-file-name-function 'my-backup-file-name)

;; Stop emacs's backup changing the file's creation date of the original file
(setq backup-by-copying t)

;; Don't create backup files
(setq make-backup-files nil)

;; Turn off mouse interface early in startup to avoid momentary display
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; User interface
(delete-selection-mode t)
(fringe-mode 0)
(global-hl-line-mode 1)
(global-superword-mode 1)
(global-undo-tree-mode 1)
(save-place-mode 1)
(savehist-mode 1)
(set-default 'truncate-lines t)
(show-paren-mode 1)
(tooltip-mode -1)
(tab-bar-mode 1)
(recentf-mode 1) ;; Store recently opened files so we can easily reopen them
(coterm-mode 1)
(setq tramp-default-method "ssh")
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
(advice-add 'shell :around #'sublemacs-core/shell-same-window-advice)
(setq auto-save-default nil) ;; stop creating those #auto-save# files
(setq confirm-kill-emacs 'y-or-n-p)
(setq create-lockfiles nil) ;; Don't create lock files
(setq frame-resize-pixelwise t) ;; For MacOS
(setq scroll-preserve-screen-position 1)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq comint-buffer-maximum-size 20000)
(setq comint-prompt-read-only t)
(setq comint-input-ignoredups t)
(setq find-file-visit-truename t) ; for magit to close diff buffer
(setq mode-require-final-newline nil)
(setq-default explicit-shell-file-name "/bin/bash")
(setq ffap-url-regexp nil)
(set-frame-parameter nil 'internal-border-width 5)
(setq require-final-newline t)
(setq backup-directory-alist `(("." . "~/.saves")))
(setq auto-window-vscroll nil) ;; Improve cursor speed. See https://emacs.stackexchange.com/questions/28736/emacs-pointcursor-movement-lag/28746
(setq persp-show-modestring nil)
;; Set tab-bar width to maximum
(setq tab-bar-auto-width-max nil)
(setq tab-bar-tab-name-function #'sublemacs-core/tab-name-function)
(setq tab-bar-close-button nil)
(setq tab-bar-new-button nil)
(setq helm-ff-skip-boring-files t)
(setq helm-ff-fuzzy-matching nil)

;; TODO: evaluate using this variable
;; Never expire cached values on remote file-name
;; (setq remote-file-name-inhibit-cache nil)

;; Only consider Git as version control system
(setq vc-handled-backends '(Git))

;; Prevent undo tree files from polluting your git repo
(setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))

;; Store more recent files
(setq recentf-max-saved-items 100)

(defalias 'yes-or-no-p 'y-or-n-p)

(setq inhibit-startup-message t)

;; Maximize window at startup
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Improve scrolling
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq use-dialog-box nil) ;; Disable dialog boxes since they weren't working in Mac OSX

;; Enable line numbers and customize their format.
(column-number-mode)

;; Enable line numbers for some modes
(dolist (mode '(text-mode-hook
                prog-mode-hook
                markdown-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (progn
                             (setq c-basic-offset 4)
                             (display-line-numbers-mode 1)))))

;; Override some modes which derive from the above
(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Flyspell / ispell
(dolist (mode '(text-mode-hook))
  (add-hook mode (lambda () (flyspell-mode))))
(dolist (mode '(prog-mode-hook))
  (add-hook mode (lambda () (flyspell-prog-mode))))

(setq ispell-current-dictionary "american")

;; Don’t warn for large files (shows up when launching videos)
(setq large-file-warning-threshold nil)

;; Don’t warn for following symlinked files
(setq vc-follow-symlinks t)

;; Don’t warn when advice is added for functions
(setq ad-redefinition-action 'accept)

;; -----------
;; -- Hooks --
;; -----------
(add-hook 'comint-output-filter-functions #'comint-truncate-buffer)

(add-hook 'shell-mode-hook
          (lambda () (progn
                       (ansi-color-for-comint-mode-on)
                       (diff-hl-mode -1)
                       (toggle-truncate-lines t)
                       (projectile-mode -1)
                       (setq comint-input-ring-file-name (concat (file-remote-p default-directory) "~/.bash_history"))
                       (comint-read-input-ring t)
                       (make-local-variable 'show-paren-mode) ;; The value of shom-paren-mode will be local to this buffer.
                       (setq show-paren-mode nil)
                       )))

;; https://www.reddit.com/r/emacs/comments/oh36yi/company_and_companyshell_super_slow_over_tramp/
(defun shell-mode-hook-setup ()
  "Set up `shell-mode'."

  ;; `company-native-complete' is better than `completion-at-point'
  (local-set-key (kbd "TAB") 'company-complete)

  ;; You need to set the variable comint-prompt-regexp to a regex that
  ;; will match your shell prompt. The following is a good default for
  ;; bash, zsh, and csh.
  (setq comint-prompt-regexp "^.+[$%>] ")

  ;; @see https://github.com/redguardtoo/emacs.d/issues/882
  (setq-local company-idle-delay 1))
(add-hook 'shell-mode-hook 'shell-mode-hook-setup)

(with-eval-after-load 'shell
  (native-complete-setup-bash))

;; ---------------------------
;; -- Setup custom packages --
;; ---------------------------
(add-hook 'helm-minibuffer-set-up-hook    #'sublemacs-core/select-whole-line)

(diff-hl-margin-mode 1)
(global-diff-hl-mode 1)
(setq persp-mode-prefix-key (kbd "C-x x"))
(setq persp-initial-frame-name "*Home*")
(persp-mode 1)
;; Order alphabetically except for persp-initial-frame-name
(defun persp-names ()
  (let ((persps (hash-table-values (perspectives-hash))))
    (sort (mapcar 'persp-name persps) (lambda (a b)
                                        (and (not (string= b persp-initial-frame-name))
                                             (string< (downcase a) (downcase b)))))))

;; -- Company --
(global-company-mode 1)
(company-posframe-mode 1)
(setq company-idle-delay 1.0)
(setq company-tooltip-minimum-width 40)
(setq company-tooltip-align-annotations t)
(setq company-tooltip-flip-when-above t)
(setq company-require-match nil)
(setq company-format-margin-function 'company-vscode-dark-icons-margin)
(add-to-list 'company-backends 'company-native-complete)

;; -- Doom --
(doom-modeline-mode 1)
(setq doom-modeline-persp-name nil)
(setq doom-modeline-workspace-name nil)
(setq doom-modeline-bar-width 8)
(setq doom-modeline-buffer-encoding 'nondefault)
(setq doom-modeline-height 40)
(setq doom-modeline-buffer-file-name-style "relative-to-project")

;; -- Helm --
(helm-mode 1)
(setq helm-M-x-fuzzy-match t)
(setq helm-always-two-windows nil)
(setq helm-buffer-max-length nil)
(setq helm-buffer-skip-remote-checking t) ;; Make helm-mini much faster
(setq helm-buffers-fuzzy-matching t)
(setq helm-completion-style 'helm-fuzzy)
(setq helm-default-display-buffer-functions '(display-buffer-in-side-window))
(setq helm-display-buffer-default-height 13)
(setq helm-display-header-line nil)
(setq helm-ff-lynx-style-map t)
(setq helm-grep-file-path-style 'relative)
(setq helm-recentf-fuzzy-match t)
(setq helm-grep-git-grep-command "git --no-pager grep -n%cH --recurse-submodules --no-color --full-name -e %p -- %f")
(add-hook 'helm-grep-mode-hook  'wgrep-change-to-wgrep-mode 100)
(add-hook 'helm-occur-mode-hook 'wgrep-change-to-wgrep-mode 100)

;; Add --numbers in ag command
(if (executable-find "ag")
    (progn
      (setq helm-grep-ag-command "ag --unrestricted --numbers -S --nocolor --nogroup %s %s %s")
      (setq helm-ag-base-command "ag --unrestricted --numbers -S --nocolor --nogroup")))

;; Allow to find formats other than :NUMBER
(defun helm-ff-goto-linum (candidate)
  "Find file CANDIDATE and maybe jump to line number found in fname at point.
Line number should be added at end of fname preceded with \":\".
E.g. \"foo:12\"."
  (let ((linum (with-helm-current-buffer
                 (let ((str (buffer-substring-no-properties
                             (end-of-thing 'filename) (point-at-eol))))
                   (when (string-match "[^0-9]*\\([0-9]+\\)" str)
                     (match-string 1 str))))))
    (find-file candidate)
    (and linum (not (string= linum ""))
         (helm-goto-line (string-to-number linum) t))))

;; Remove (string-match-p ":\\([0-9]+:?\\)" str-at-point) check from
;; minibuffer
(defun helm-find-files-action-transformer (actions candidate)
  "Action transformer for `helm-source-find-files'."
  (let ((str-at-point (with-helm-current-buffer
                        (buffer-substring-no-properties
                         (point-at-bol) (point-at-eol)))))
    (when (file-regular-p candidate)
      (setq actions (helm-append-at-nth
                     actions '(("Checksum File" . helm-ff-checksum)) 4)))
    (cond ((and (file-exists-p candidate)
                (string-match helm-ff--trash-directory-regexp
                              (helm-basedir (expand-file-name candidate)))
                (not (member (helm-basename candidate) '("." "..")))
                (executable-find "trash"))
           (helm-append-at-nth
            actions
            '(("Restore file(s) from trash" . helm-restore-file-from-trash)
              ("Delete file(s) from trash" . helm-ff-trash-rm))
            1))
          ((and helm--url-regexp
                (not (string-match-p helm--url-regexp str-at-point))
                (not (with-helm-current-buffer (eq major-mode 'dired-mode))))
           (append '(("Find file to line number" . helm-ff-goto-linum))
                   actions))
          ((string-match (image-file-name-regexp) candidate)
           (helm-append-at-nth
            actions
            '(("Rotate image right `M-r'" . helm-ff-rotate-image-right)
              ("Rotate image left `M-l'" . helm-ff-rotate-image-left)
              ("Start slideshow with marked" . helm-ff-start-slideshow-on-marked))
            3))
          ((string-match "\\.el\\'" candidate)
           (helm-append-at-nth
            actions
            '(("Byte compile lisp file(s) `M-B, C-u to load'"
               . helm-find-files-byte-compile)
              ("Load File(s) `M-L'" . helm-find-files-load-files))
            2))
          ((string-match (concat (regexp-opt load-suffixes) "\\'") candidate)
           (helm-append-at-nth
            actions
            '(("Load File(s) `M-L'" . helm-find-files-load-files))
            2))
          ((and (string-match "\\.html?$" candidate)
                (file-exists-p candidate))
           (helm-append-at-nth
            actions '(("Browse url file" . browse-url-of-file)) 2))
          (t actions))))

;; For whatever reason, in terminal mode Helm-mini (and other Helm
;; based minibuffer) doesn't go to recentf results. However, by
;; disabling this feature, it does. As a consequence, minibuffer
;; cursor doesn't not wrap around when it's at the end of the result
;; list.
(setq helm-move-to-line-cycle-in-source nil)

;; -- Magit --
(setq magit-diff-refine-hunk 'all)
(setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
(setq magit-save-repository-buffers nil)

(put 'magit-log-mode 'magit-log-default-arguments
     '("--graph" "-n256" "--decorate" "--color"))

;; Inspired by https://scripter.co/narrowing-the-author-column-in-magit/
(use-package magit-log
  :init
  (progn
    ;; (setq magit-log-margin '(t age magit-log-margin-width t 18)) ;Default value
    (setq magit-log-margin '(t age-abbreviated magit-log-margin-width :author 11)))
  :config
  (progn
    ;; Abbreviate author name. I added this so that I can view Magit log without
    ;; too much commit message truncation even on narrow screens (like on phone).
    (defun modi/magit-log--abbreviate-author (&rest args)
      "The first arg is AUTHOR, abbreviate it.
First Middle Last -> F M Last
First.Middle.Last -> F.M Last
First Last  -> F Last
First.Last  -> F Last
Last, First -> F Last
First       -> First (no change).

It is assumed that the author has only one or two names."
      ;; ARGS               -> '((REV AUTHOR DATE))
      ;; (car ARGS)         -> '(REV AUTHOR DATE)
      ;; (nth 1 (car ARGS)) -> AUTHOR
      (let* ((author (nth 1 (car args)))
             (author-abbr (if (string-match-p "," author)
                              ;; Last, First -> F Last
                              (replace-regexp-in-string "\\(.*?\\), *\\(.\\).*" "\\2 \\1" author)
                            ;; First Last -> F Last
                            (replace-regexp-in-string "\\(.\\).*?\\(?:[. ]*?\\( .\\).*?\\)?[. ]+\\(.*\\)" "\\1\\2 \\3" author))))
        (setf (nth 1 (car args)) author-abbr))
      (car args))                       ;'(REV AUTHOR-ABBR DATE)
    (advice-add 'magit-log-format-margin :filter-args #'modi/magit-log--abbreviate-author)))


;; Override magit-display-buffer-same-window-except-diff-v1
;; With this change, it adds an exception: do the split if the height
;; is smaller than 30 lines
(defun magit-display-buffer-same-window-except-diff-v1 (buffer)
  "Display BUFFER in the selected window except for some modes.
If a buffer's `major-mode' derives from `magit-diff-mode' or
`magit-process-mode', display it in another window.  Display all
other buffers in the selected window."
  (display-buffer
   buffer (if (with-current-buffer buffer
                (derived-mode-p 'magit-diff-mode 'magit-process-mode))
              '(display-buffer-below-selected (window-min-height . 30))
            '(display-buffer-same-window))))

;; https://www.emacswiki.org/emacs/misc-cmds.el
;; Candidate as a replacement for `kill-buffer', at least when used interactively.
;; For example: (define-key global-map [remap kill-buffer] 'kill-buffer-and-its-windows)
;;
;; We cannot just redefine `kill-buffer', because some programs count on a
;; specific other buffer taking the place of the killed buffer (in the window).
;;;###autoload
(defun kill-buffer-and-its-windows (buffer &optional msgp)
  "Kill BUFFER and delete its windows.  Default is `current-buffer'.
BUFFER may be either a buffer or its name (a string)."
  (interactive (list (read-buffer "Kill buffer: " (current-buffer) 'existing) 'MSGP))
  (setq buffer  (get-buffer buffer))
  (if (buffer-live-p buffer)            ; Kill live buffer only.
      (let ((wins  (get-buffer-window-list buffer nil t))) ; On all frames.
        (when (and (buffer-modified-p buffer)
                   (fboundp '1on1-flash-ding-minibuffer-frame))
          (1on1-flash-ding-minibuffer-frame t)) ; Defined in `oneonone.el'.
        (when (kill-buffer buffer)      ; Only delete windows if buffer killed.
          (dolist (win  wins)           ; (User might keep buffer if modified.)
            (when (window-live-p win)
              ;; Ignore error, in particular,
              ;; "Attempt to delete the sole visible or iconified frame".
              (condition-case nil (delete-window win) (error nil))))))
    (when msgp (error "Cannot kill buffer.  Not a live buffer: `%s'" buffer))))

;; Edited from https://emacs.stackexchange.com/questions/35775/how-to-kill-magit-diffs-buffers-on-quit
(defun kill-magit-diff-buffer-in-current-repo (&rest _)
  "Delete the magit-diff buffer related to the current repo"
  (let ((magit-diff-buffer-in-current-repo (magit-mode-get-buffer 'magit-diff-mode)))
    (kill-buffer-and-its-windows magit-diff-buffer-in-current-repo)))

;;
;; When 'C-c C-c' is pressed in the magit commit message buffer,
;; delete the magit-diff buffer related to the current repo.
;;
(add-hook 'git-commit-setup-hook
          (lambda ()
            (add-hook 'with-editor-post-finish-hook
                      #'kill-magit-diff-buffer-in-current-repo
                      nil t))) ; the t is important

(advice-add 'magit-log-select-pick :after #'kill-magit-diff-buffer-in-current-repo)

;; Add submodule update recursively
(transient-append-suffix 'magit-submodule "a"
  '("U" "Update all (recursively)"
    sublemacs-core/magit-submodule-update-recursive))

;; Add ignored files in magit status
(sublemacs-core/insert-into-list
 magit-status-sections-hook
 'sublemacs-core/magit-insert-ignored-files
 (cl-position 'magit-insert-untracked-files
              magit-status-sections-hook))

(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
(add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)

;; Speed up magit-status (https://jakemccrary.com/blog/2020/11/14/speeding-up-magit/)
(remove-hook 'magit-status-sections-hook 'magit-insert-tags-header)
(remove-hook 'magit-status-sections-hook 'magit-insert-status-headers)

(defun repolist-status-select (&optional _button)
  "Show the status for the repository at point."
  (if-let* ((abs-path (expand-file-name (tabulated-list-get-id)))
            (tab-name (aref (tabulated-list-get-entry) 0)))
      (progn
	    (subl-run-git-repo abs-path (string-replace "\\" ":" tab-name)))))

(setq magit-repolist-columns
      '(("Name"    25 magit-repolist-column-ident ())
        ("Path"    99 magit-repolist-column-path  ())))

(add-function :before (symbol-function 'magit-repolist-status) #'repolist-status-select)

;; -- Projectile --
(projectile-mode 1)
(setq projectile-git-command "git ls-files -z --exclude-standard --recurse-submodules")
(if (and projectile-fd-executable
         (executable-find projectile-fd-executable)
         (version< (cadr (split-string (shell-command-to-string (concat projectile-fd-executable " --version")) "[ \n]+" t)) "8.3.0"))
    (progn
      ;; Remove --strip-cwd-prefix from commands as fdfind 8.2.x, ie. the
      ;; last one supported by Debian 11, doesn't have this.
      ;; For more info, see: https://github.com/bbatsov/projectile/issues/1788
      (setq projectile-generic-command (concat projectile-fd-executable " . -0 --type f --color=never"))
      (setq projectile-git-fd-args "-H -0 -E .git -tf -c never")))

(setq projectile-use-git-grep t)
(setq projectile-mode-line "Projectile")
(setq projectile-enable-caching t)

;; (defadvice projectile-project-root (around ignore-remote first activate)
;;     (unless (file-remote-p default-directory) ad-do-it))


;; Speedup projectile over tramp
;; https://github.com/bbatsov/projectile/issues/234#issuecomment-37756785
(defadvice projectile-on (around exlude-tramp activate)
    (unless  (--any? (and it (file-remote-p it))
        (list
            (buffer-file-name)
            list-buffers-directory
            default-directory))
    ad-do-it))

(setq verilog-auto-indent-on-newline nil)
(setq verilog-auto-newline nil)
(setq verilog-highlight-max-lookahead 1000)
(setq verilog-indent-level 4)
(setq verilog-indent-level-module      0)
(setq verilog-indent-level-declaration 4)
(setq verilog-indent-level-behavioral 0)
(setq verilog-indent-level-directive 0)
(setq verilog-indent-begin-after-if nil)
(add-to-list 'auto-mode-alist '("\\.sv" . verilog-mode))
(add-to-list 'auto-mode-alist '("\\.v"  . verilog-mode))
(add-to-list 'auto-mode-alist '("\\.vh" . verilog-mode))
(add-to-list 'auto-mode-alist '("\\.svh" . verilog-mode))
(eval-after-load 'verilog-mode (electric-indent-mode -1))

(add-to-list 'interpreter-mode-alist '("#!/bin/bash" . sh-mode))

(add-to-list 'auto-mode-alist '("\\.sdc" . tcl-mode))

;; Automatically use vlf for large files
(setq vlf-application 'dont-ask)

(setq-default mc/cmds-to-run-for-all
      '(
        helm-confirm-and-exit-minibuffer
        indent-for-tab-command
        sgml-slash
        ))

(setq-default mc/cmds-to-run-once
      '(sublemacs-core/mc-cursor-up
        sublemacs-core/mc-cursor-down
        cua-cut-region
        cua-copy-region
        cua-paste
        sublemacs-core/copy
        sublemacs-core/cut
        sublemacs-core/paste
        sublemacs-core/save-buffer
        mc/insert-letters
        mc/insert-numbers
        sublemacs-core/mc-execute))

(setq mc/always-run-for-all t)

(multiple-cursors-mode 1)

;; ------------------
;; -- Key bindings --
;; ------------------
(ld "sublemacs-keybindings.el")

;; ----------------
;; -- Appearance --
;; ----------------

;; Load theme
(ld "sublemacs-theme.el")

;; -------------------------
;; -- Runtime Performance --
;; -------------------------
;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;; -------------
;; -- Startup --
;; -------------
(defun subl-run-git-repo (rel-path name)
  (if (member name (persp-all-names))
      (persp-switch name)
    (progn
      (persp-switch name)
      (let ((default-directory (expand-file-name rel-path)))
	    (progn
	      (sublemacs-core/shell (concat "shell-local (" name ")") )
	      (if (string= (substring (shell-command-to-string "git rev-parse --is-inside-work-tree") 0 -1) "true")
	          (progn
		        (sublemacs-core/split-window-horizontally)
		        (magit-status)
		        (windmove-left)
		        (if (file-exists-p ".brige")
		            (progn
                      (let ((shell-local-bufname (buffer-name))
                            (shell-cluster-default-directory (shell-command-to-string "brige info cluster --remote"))
                            (shell-app-default-directory     (shell-command-to-string "brige info app --remote")))
                        (progn
		                  (sublemacs-core/split-window-vertically)

                          ;; app
                          (if (> (length shell-app-default-directory) 0)
		                      (let ((default-directory shell-app-default-directory))  (sublemacs-core/shell (concat "shell-app (" name ")"))))


                          ;; cluster
                          (if (> (length shell-cluster-default-directory) 0)
                              (progn
                                (switch-to-buffer shell-local-bufname)
		                        (let ((default-directory shell-cluster-default-directory))  (sublemacs-core/shell (concat "shell-cluster (" name ")")))))
                        )
                    ))
		          )
	            )
	        )
	      )
        )
      )
    )
  )

(add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)

(setq lazy-hightlight-interval 1)
(setq lazy-highlight-cleanup nil)

(add-to-list 'tramp-remote-path 'tramp-own-remote-path)


(setq forge-topic-list-limit '(20 . 5))

;; Automatically jump tp pullreq if browsing at point
(add-to-list 'magit-dwim-selection '(forge-browse-pullreq nil t))

(defun aorst/font-installed-p (font-name)
  "Check if font with FONT-NAME is available."
  (if (find-font (font-spec :name font-name))
      t
    nil))
(use-package all-the-icons
  :config
  (when (and (not (aorst/font-installed-p "all-the-icons"))
             (window-system))
    (all-the-icons-install-fonts t)))

(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(setq dired-listing-switches "-alh")
(setq dired-du-size-format t)
(add-hook 'dired-mode-hook 'diff-hl-dired-mode-unless-remote)

(defun helm-ag--init ()
  "Not documented."
  (let ((buf-coding buffer-file-coding-system))
    (message "helm-ag--init")
    (helm-attrset 'recenter t)
    (with-current-buffer (helm-candidate-buffer 'global)
      (let* ((default-directory (or helm-ag--default-directory
                                    default-directory))
             (cmds (helm-ag--construct-command (helm-attr 'search-this-file)))
             (coding-system-for-read buf-coding)
             (coding-system-for-write buf-coding))
        (setq helm-ag--ignore-case (helm-ag--ignore-case-p cmds helm-ag--last-query)
              helm-ag--last-command cmds)
        (let ((ret (apply #'process-file (car cmds) nil t nil (cdr cmds))))
          (message "Try: %s" cmds)
          (if (zerop (length (buffer-string)))
              (message "No ag output: '%s' %s" helm-ag--last-query cmds)
            (unless (helm-ag--command-succeeded-p ret)
              (unless (executable-find (car cmds))
                (error "'%s' is not installed" (car cmds)))
              (error "Failed: '%s'" helm-ag--last-query))))
        (when helm-ag--buffer-search
          (helm-ag--abbreviate-file-name))
        (helm-ag--remove-carrige-returns)
        (helm-ag--save-current-context)))))

(setq org-support-shift-select t)

;; Make columns larger on profiler report
(setf (caar profiler-report-cpu-line-format) 100
      (caar profiler-report-memory-line-format) 100)

;; Enable pdf-tools
(if (display-graphic-p)
    (progn
      (require 'pdf-tools)
      (pdf-tools-install)))

;; Startup setup
(defun sublemacs/setup ()
  (interactive)
  (nerd-icons-install-fonts))

;; Disable debug-on-error
(setq debug-on-error nil)

;; Setup custom.el file...
(ld "custom.el")

(sublemacs-core/shell (concat "shell-local (" persp-initial-frame-name ")"))
(sublemacs-core/split-window-horizontally)
(magit-list-repositories)

(setq image-auto-resize-on-window-resize nil)
(add-to-list 'auto-mode-alist '("\\.svg" . image-mode))

;; Turn off bell sound
(setq ring-bell-function 'ignore)

;; -- END --
