;; Disable minor mode keystrokes that are used as global
(with-eval-after-load 'magit '(progn
                                (define-key magit-mode-map (kbd "C-w") nil)
                                (define-key magit-mode-map (kbd "M-p") nil)
                                (define-key magit-mode-map (kbd "M-w") nil)))

(define-key magit-status-mode-map (kbd "M-p") nil)
(define-key magit-status-mode-map (kbd "C-w") nil)
(define-key magit-status-mode-map (kbd "M-w") nil)
(define-key magit-status-mode-map (kbd "M-S") 'sublemacs-core/magit-status-up)

(define-key magit-mode-map (kbd "C-s") 'transient-save)

(eval-after-load "org"
  '(setq org-mode-map (make-sparse-keymap)))

(eval-after-load "flyspell"
  '(progn
     (define-key flyspell-mouse-map [down-mouse-3] nil)
     (define-key flyspell-mouse-map [mouse-3] nil)
     (define-key flyspell-mouse-map [down-mouse-2] nil)
     (define-key flyspell-mouse-map [mouse-2] nil)
     (define-key flyspell-mode-map flyspell-auto-correct-binding nil)
     (define-key flyspell-mode-map [(control ?\,)] nil)
     (define-key flyspell-mode-map [(control ?\.)] nil)
     (define-key flyspell-mode-map [?\C-c ?$] nil)
))

;; Ignore all key bindings for undo-tree
(eval-after-load 'undo-tree
    '(progn
        (define-key undo-tree-map (kbd "C-/")                nil)))

;; Ignore all key bindings for verilog-mode
(eval-after-load 'verilog-mode
    '(progn
        (define-key verilog-mode-map (kbd ";")                nil)
        (define-key verilog-mode-map (kbd ":")                nil)
        (define-key verilog-mode-map (kbd "`")                nil)
        (define-key verilog-mode-map (kbd "\t")               nil)
        (define-key verilog-mode-map (kbd "\r")               nil)
        (define-key verilog-mode-map (kbd "\M-\C-b")          nil)
        (define-key verilog-mode-map (kbd "\M-\C-f")          nil)
        (define-key verilog-mode-map (kbd "\M-\r")            nil)
        (define-key verilog-mode-map (kbd "\M-\t")            nil)
        (define-key verilog-mode-map (kbd "\M-?")             nil)
        (define-key verilog-mode-map (kbd "\C-c`")            nil)
        (define-key verilog-mode-map (kbd "\C-v")             nil)
        (define-key verilog-mode-map (kbd "C-;")              nil)
        (define-key verilog-mode-map (kbd "C-:")              nil)
        (define-key verilog-mode-map (kbd "\C-c*")            nil)
        (define-key verilog-mode-map (kbd "\C-c?")            nil)
        (define-key verilog-mode-map (kbd "\C-c\C-r")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-i")         nil)
        (define-key verilog-mode-map (kbd "\C-c=")            nil)
        (define-key verilog-mode-map (kbd "\C-c\C-b")         nil)
        (define-key verilog-mode-map (kbd "\C-c/")            nil)
        (define-key verilog-mode-map (kbd "\C-c\C-c")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-u")         nil)
        (define-key verilog-mode-map (kbd "\M-\C-a")          nil)
        (define-key verilog-mode-map (kbd "\M-\C-e")          nil)
        (define-key verilog-mode-map (kbd "\C-c\C-d")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-k")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-a")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-s")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-p")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-z")         nil)
        (define-key verilog-mode-map (kbd "\C-c\C-e")         nil)
        (define-key verilog-mode-map (kbd "TAB")              nil)
        (define-key verilog-mode-map (kbd "M-RET")            nil)
        (define-key verilog-mode-map (kbd "\C-c\C-h")         nil)))

;; Unset key bindings for cua-mode
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x"))
(global-unset-key (kbd "C-c"))

;; Unset multiple cursor keybindings
(define-key mc/keymap (kbd "<return>") nil)

;; Unset comint/shell keybindings
(define-key shell-mode-map (kbd "M-p") nil)
(define-key shell-mode-map (kbd "C-d") nil)

;; Global
(global-set-key (kbd "C-S-<down>")    #'drag-stuff-down)
(global-set-key (kbd "C-S-<up>")      #'drag-stuff-up)
(global-set-key (kbd "C-S-f")         'sublemacs-core/git-grep)
(global-set-key (kbd "<C-S-f>")       'sublemacs-core/git-grep)
(global-set-key (kbd "M-S-f")         'sublemacs-core/git-grep-this-repo)
(global-set-key (kbd "M-F")           'sublemacs-core/git-grep-this-repo)
(global-set-key (kbd "M-f")           'sublemacs-core/grep-this-directory)
(global-set-key (kbd "C-S-p")         'helm-M-x)
(global-set-key (kbd "<C-S-p>")       'helm-M-x)
(global-set-key (kbd "C-x p")         'sublemacs-core/copy-current-buffer-filename)
(global-set-key (kbd "C-a")           'mark-whole-buffer)
(global-set-key (kbd "C-d")           'sublemacs-core/dired)
(global-set-key (kbd "C-g")           'sublemacs-core/keyboard-quit)
(global-set-key (kbd "C-b")           'helm-mini)
(global-set-key (kbd "C-f")           'sublemacs-core/swoop)
(global-set-key (kbd "C-o")           'find-file)
(global-set-key (kbd "C-p")           'sublemacs-core/projectile-find-file)
(global-set-key (kbd "M-p")           'sublemacs-core/projectile-find-file-this-repo) ;; Find on this-repo
(global-set-key (kbd "C-s")           'sublemacs-core/save-buffer)
(global-set-key (kbd "C-w")           'kill-this-buffer)
(global-set-key (kbd "C-,")           'previous-buffer)
(global-set-key (kbd "C-.")           'next-buffer)
(global-set-key (kbd "M-[ ,")         'previous-buffer)
(global-set-key (kbd "M-[ .")         'next-buffer)
(global-set-key (kbd "M-,")           'goto-last-change)
(global-set-key (kbd "M-.")           'goto-last-change-reverse)
(global-set-key (kbd "C-y")           'undo-tree-redo)
(global-set-key (kbd "C-z")           'undo-tree-undo)
(global-set-key (kbd "M-w")           'delete-window)
(global-set-key (kbd "C-n")           'sublemacs-core/narrow-or-widen)
(global-set-key (kbd "M-s")           'magit-status)
(global-set-key (kbd "C-j")           'sublemacs-core/ffap)
(global-set-key (kbd "C-l")           'sublemacs-core/copy)    ;; C-c after keyboard-translate
(global-set-key (kbd "C-k")           'sublemacs-core/cut)     ;; C-x after keyboard-translate
(global-set-key (kbd "C-v")           'sublemacs-core/paste)
(global-set-key (kbd "C-/")           'comment-or-uncomment-region)
(global-set-key (kbd "<right>")       'sublemacs-core/right-char)
(global-set-key (kbd "S-<right>")     'sublemacs-core/shift-right-char)
(global-set-key (kbd "<left>")        'sublemacs-core/left-char)
(global-set-key (kbd "S-<left>")      'sublemacs-core/shift-left-char)
(global-set-key (kbd "<end>")         'move-end-of-line)
(global-set-key (kbd "<home>")        'move-beginning-of-line)
(global-set-key (kbd "M-<right>")     'windmove-right)
(global-set-key (kbd "M-<left>")      'windmove-left)
(global-set-key (kbd "M-<up>")        'windmove-up)
(global-set-key (kbd "M-<down>")      'windmove-down)
(global-set-key (kbd "M-o")           'sublemacs-core/split-window-vertically)
(global-set-key (kbd "M-x")           'sublemacs-core/toggle-maximize-buffer)
(global-set-key (kbd "M-i")           'sublemacs-core/split-window-horizontally)
(global-set-key (kbd "C-<prior>")     'sublemacs-core/previous-hunk)
(global-set-key (kbd "C-<next>")      'sublemacs-core/next-hunk)
(global-set-key (kbd "C-t")           'sublemacs-core/shell)
(global-set-key (kbd "M-S-<left>")    'sublemacs/persp-prev)
(global-set-key (kbd "M-S-<right>")   'sublemacs/persp-next)
(global-set-key (kbd "M-S-<up>")      'sublemacs-core/mc-cursor-up)
(global-set-key (kbd "M-S-<down>")    'sublemacs-core/mc-cursor-down)
(global-set-key (kbd "M-RET")         'mc/mark-all-like-this)
(global-set-key (kbd "C-S-l")         'mc/edit-lines)
(global-set-key (kbd "<C-S-l>")       'mc/edit-lines)
(global-set-key (kbd "C-x ,")         'widen)            ;; C-k , after keyboard-translate
(global-set-key (kbd "C-x .")         'narrow-to-region) ;; C-k . after keyboard-translate
(global-set-key (kbd "C-RET")         'cua-toggle-rectangle-mark)
(global-set-key (kbd "C-;")           'er/expand-region)
(global-set-key (kbd "C-:")           'er/contract-region)
(global-set-key (kbd "C-e")           'sublemacs-core/mc-execute)
(global-set-key (kbd "C-SPC")         'other-window)
(global-set-key (kbd "C-'")           'cua-set-mark)

(define-key helm-map (kbd "C-r")     'helm-minibuffer-history)
(define-key helm-map (kbd "<left>")  (lookup-key global-map (kbd "<left>")))
(define-key helm-map (kbd "<right>") (lookup-key global-map (kbd "<right>")))
(define-key helm-map (kbd "C-z")     (lookup-key global-map (kbd "C-z")))
(define-key helm-map (kbd "C-k")     (lookup-key global-map (kbd "C-k")))
(define-key helm-map (kbd "C-l")     (lookup-key global-map (kbd "C-l")))
(define-key helm-map (kbd "C-v")     (lookup-key global-map (kbd "C-v")))
(define-key helm-grep-map (kbd "<left>")  (lookup-key global-map (kbd "<left>")))
(define-key helm-grep-map (kbd "<right>") (lookup-key global-map (kbd "<right>")))
(define-key helm-grep-map (kbd "C-z")     (lookup-key global-map (kbd "C-z")))
(define-key helm-grep-map (kbd "C-k")     (lookup-key global-map (kbd "C-k")))
(define-key helm-grep-map (kbd "C-l")     (lookup-key global-map (kbd "C-l")))
(define-key helm-grep-map (kbd "C-v")     (lookup-key global-map (kbd "C-v")))

;; Terminal hooks for Control+Shift
(add-hook 'term-setup-hook
          (lambda ()
             (define-key function-key-map "\e[1;AA"  [C-S-a])
             (define-key function-key-map "\e[1;BB"  [C-S-b])
             (define-key function-key-map "\e[1;CC"  [C-S-c])
             (define-key function-key-map "\e[1;DD"  [C-S-d])
             (define-key function-key-map "\e[1;EE"  [C-S-e])
             (define-key function-key-map "\e[1;FF"  [C-S-f])
             (define-key function-key-map "\e[1;GG"  [C-S-g])
             (define-key function-key-map "\e[1;HH"  [C-S-h])
             (define-key function-key-map "\e[1;II"  [C-S-i])
             (define-key function-key-map "\e[1;JJ"  [C-S-j])
             (define-key function-key-map "\e[1;KK"  [C-S-k])
             (define-key function-key-map "\e[1;LL"  [C-S-l])
             (define-key function-key-map "\e[1;MM"  [C-S-m])
             (define-key function-key-map "\e[1;NN"  [C-S-n])
             (define-key function-key-map "\e[1;OO"  [C-S-o])
             (define-key function-key-map "\e[1;PP"  [C-S-p])
             (define-key function-key-map "\e[1;QQ"  [C-S-q])
             (define-key function-key-map "\e[1;RR"  [C-S-r])
             (define-key function-key-map "\e[1;SS"  [C-S-s])
             (define-key function-key-map "\e[1;TT"  [C-S-t])
             (define-key function-key-map "\e[1;UU"  [C-S-u])
             (define-key function-key-map "\e[1;VV"  [C-S-v])
             (define-key function-key-map "\e[1;WW"  [C-S-w])
             (define-key function-key-map "\e[1;XX"  [C-S-x])
             (define-key function-key-map "\e[1;YY"  [C-S-y])
             (define-key function-key-map "\e[1;ZZ"  [C-S-z])))

;; Translate C-x/C-c to C-k/C-l :troll:
(define-key key-translation-map [?\C-x] [?\C-k])
(define-key key-translation-map [?\C-c] [?\C-l])
(define-key key-translation-map [?\C-k] [?\C-x])
(define-key key-translation-map [?\C-l] [?\C-c])

;; Ingnore tabulated keybindings
(eval-after-load 'magit
  '(progn
        (define-key tabulated-list-mode-map (kbd "M-<left>")  (lookup-key global-map (kbd "M-<left>")))
        (define-key tabulated-list-mode-map (kbd "M-<right>") (lookup-key global-map (kbd "M-<right>")))
        (define-key tabulated-list-mode-map (kbd "M-<up>")    (lookup-key global-map (kbd "M-<up>")))
        (define-key tabulated-list-mode-map (kbd "M-<down>")  (lookup-key global-map (kbd "M-<down>")))
))

;; Enable cua mode
(cua-mode t)

(defun cua-cut-region (arg)  (interactive "P") (sublemacs-core/cut arg))
(defun cua-copy-region (arg) (interactive "P") (sublemacs-core/copy arg))
(defun cua-paste (arg)       (interactive "P") (sublemacs-core/paste arg))

;; Code review
(define-key forge-pullreq-section-map (kbd "RET") 'code-review-forge-pr-at-point)
(define-key forge-pullreq-section-map (kbd "C-j") 'forge-browse-pullreq)

;; pdf-tools
(eval-after-load 'pdf-view-mode
  '(progn
     (define-key pdf-view-mode-map (kbd "C-f") 'isearch-forward-regexp)
     (define-key isearch-mode-map (kbd "<up>") 'isearch-repeat-backward)
     (define-key isearch-mode-map (kbd "<down>") 'isearch-repeat-forward)))

;; Undefine some dired keybindings
(define-key dired-mode-map (kbd "C-o") nil)
(define-key dired-mode-map (kbd "M-s") nil)
(define-key dired-mode-map (kbd "C-S-<up>") 'dired-up-directory)
(define-key dired-mode-map (kbd "-")        'dired-up-directory) ;; Vim-like alternative to go up dir
(define-key dired-mode-map (kbd "C-S-c")    'sublemacs-core/dired-copy)
(define-key dired-mode-map (kbd "C-S-x")    'sublemacs-core/dired-cut)
(define-key dired-mode-map (kbd "C-S-v")    'sublemacs-core/dired-paste)

(eval-after-load 'wgrep-mode
  '(progn
     (define-key wgrep-mode-map (kbd "C-S") 'wgrep-finish-edit)))

(eval-after-load 'evil-mode
  '(progn
     ;; Evil mode
     ;; by default, evil-mode overwrites our wonderul swoop with vim's "jump forward"
     ;; here, we disable this and get back our good old swoop
     (define-key evil-normal-state-map (kbd "C-'") 'cua-set-mark)
     (define-key evil-insert-state-map (kbd "C-'") 'cua-set-mark)
     (define-key evil-motion-state-map (kbd "C-'") 'cua-set-mark)
     (define-key evil-visual-state-map (kbd "C-'") 'cua-set-mark)

     (define-key evil-normal-state-map (kbd "C-SPC") 'other-window)
     (define-key evil-insert-state-map (kbd "C-SPC") 'other-window)
     (define-key evil-motion-state-map (kbd "C-SPC") 'other-window)
     (define-key evil-visual-state-map (kbd "C-SPC") 'other-window)

     (define-key evil-normal-state-map (kbd "C-f")   'sublemacs-core/swoop)
     (define-key evil-insert-state-map (kbd "C-f")   'sublemacs-core/swoop)
     (define-key evil-motion-state-map (kbd "C-f")   'sublemacs-core/swoop)

     (define-key evil-normal-state-map (kbd "C-b")   'helm-mini)
     (define-key evil-insert-state-map (kbd "C-b")   'helm-mini)
     (define-key evil-motion-state-map (kbd "C-b")   'helm-mini)

     (define-key evil-normal-state-map (kbd "C-p")   'sublemacs-core/projectile-find-file)
     (define-key evil-insert-state-map (kbd "C-p")   'sublemacs-core/projectile-find-file)
     (define-key evil-motion-state-map (kbd "C-p")   'sublemacs-core/projectile-find-file)

     (define-key evil-normal-state-map (kbd "C-d")   'sublemacs-core/dired)
     (define-key evil-insert-state-map (kbd "C-d")   'sublemacs-core/dired)
     (define-key evil-motion-state-map (kbd "C-d")   'sublemacs-core/dired)

     (define-key evil-normal-state-map (kbd "C-S-v") 'evil-visual-block)

     (define-key evil-normal-state-map (kbd "C-o") 'find-file)
     (define-key evil-insert-state-map (kbd "C-o") 'find-file)
     (define-key evil-motion-state-map (kbd "C-o") 'find-file)
     (define-key evil-visual-state-map (kbd "C-o") 'find-file)

     (define-key evil-normal-state-map (kbd "C-,") 'previous-buffer)
     (define-key evil-insert-state-map (kbd "C-,") 'previous-buffer)
     (define-key evil-motion-state-map (kbd "C-,") 'previous-buffer)
     (define-key evil-visual-state-map (kbd "C-,") 'previous-buffer)
     (define-key evil-normal-state-map (kbd "C-.") 'next-buffer)
     (define-key evil-insert-state-map (kbd "C-.") 'next-buffer)
     (define-key evil-motion-state-map (kbd "C-.") 'next-buffer)
     (define-key evil-visual-state-map (kbd "C-.") 'next-buffer)

     (define-key evil-normal-state-map (kbd "M-,") 'goto-last-change)
     (define-key evil-insert-state-map (kbd "M-,") 'goto-last-change)
     (define-key evil-motion-state-map (kbd "M-,") 'goto-last-change)
     (define-key evil-visual-state-map (kbd "M-,") 'goto-last-change)
     (define-key evil-normal-state-map (kbd "M-.") 'goto-last-change-reverse)
     (define-key evil-insert-state-map (kbd "M-.") 'goto-last-change-reverse)
     (define-key evil-motion-state-map (kbd "M-.") 'goto-last-change-reverse)
     (define-key evil-visual-state-map (kbd "M-.") 'goto-last-change-reverse)

     ;; Remapping "escape" keys
     ;; (evil-set-toggle-key "C-\\") ;; Not working for some reason...
     (define-key evil-emacs-state-map  (kbd "C-`") 'evil-normal-state)
     (define-key evil-normal-state-map (kbd "C-`") 'evil-emacs-state)

     ;; Vim-mode : Use C-g or <escape> to go back to normal state
     (define-key evil-insert-state-map (kbd "<escape>") 'evil-normal-state)
     (define-key evil-motion-state-map (kbd "<escape>") 'evil-normal-state)
     (define-key evil-visual-state-map (kbd "<escape>") 'evil-normal-state)
     (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
     (define-key evil-motion-state-map (kbd "C-g") 'evil-normal-state)
     (define-key evil-visual-state-map (kbd "C-g") 'evil-normal-state)

     ;; In normal state, scroll with space / shift-space
     (define-key evil-normal-state-map (kbd "SPC") 'View-scroll-half-page-forward)
     (define-key evil-insert-state-map (kbd "SPC") 'self-insert-command)
     (define-key evil-visual-state-map (kbd "SPC") 'self-insert-command)
     (define-key evil-motion-state-map (kbd "SPC") 'self-insert-command)
     (define-key evil-normal-state-map (kbd "S-SPC") 'View-scroll-half-page-backward)
     (define-key evil-insert-state-map (kbd "S-SPC") 'self-insert-command)
     (define-key evil-visual-state-map (kbd "S-SPC") 'self-insert-command)
     (define-key evil-motion-state-map (kbd "S-SPC") 'self-insert-command)

     ;; Choosing the stating evil state for each major mode
     (evil-set-initial-state 'shell-mode        'emacs)
     (evil-set-initial-state 'fundamental-mode  'emacs)
     (evil-set-initial-state 'dired-mode        'emacs)
     (evil-set-initial-state 'magit-status-mode 'emacs)
     (evil-set-initial-state 'verilog-mode      'normal)
     (evil-set-initial-state 'lisp-mode         'normal)
     (evil-set-initial-state 'tcl-mode          'normal)))


(add-hook 'codal-mode-hook
          (lambda ()
            (local-set-key (kbd "C-d") nil)))

(add-hook 'c++-mode-hook
          (lambda ()
            (local-set-key (kbd "C-d") nil)))

(add-hook 'cc-mode-hook
          (lambda ()
            (local-set-key (kbd "C-d") nil)))

(add-hook 'c-mode-hook
          (lambda ()
            (local-set-key (kbd "C-d") nil)))
