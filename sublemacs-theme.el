(load-theme 'doom-nord t)

(setq subl-face-default-fg (doom-blend (face-foreground 'default) "#667777" 0.90))
(setq subl-face-default-bg (doom-blend (face-background 'default) "#220000" 0.85))
(setq subl-face-hl-line-bg (doom-blend (face-foreground 'default)  (face-background 'default) 0.05))
(setq subl-face-truncation-fg (doom-blend subl-face-default-fg subl-face-default-bg 0.40))

(setq subl-face-diff-added-fg           (doom-blend (face-foreground 'diff-added)    (face-background 'default) 0.7))
(setq subl-face-diff-removed-fg         (doom-blend (face-foreground 'diff-removed)  (face-background 'default) 0.7))
(setq subl-face-diff-refine-added-bg    (doom-blend subl-face-diff-added-fg          (face-background 'default) 0.01))
(setq subl-face-diff-refine-removed-bg  (doom-blend subl-face-diff-removed-fg        (face-background 'default) 0.01))

(setq subl-face-mode-line-bg          (doom-blend (face-background 'default) "#000011" 0.20))
(setq subl-face-mode-line-inactive-bg (doom-blend (face-background 'default) "#000011" 0.65))

;; General
(set-face-attribute 'default                  nil :inherit nil :background subl-face-default-bg :foreground subl-face-default-fg)
(set-face-attribute 'show-paren-match         nil :background 'unspecified :foreground 'unspecified :weight 'unspecified :underline t)
(set-face-attribute 'hl-line                  nil :background subl-face-hl-line-bg)
(set-face-attribute 'line-number              nil :inherit 'default)
(set-face-attribute 'vertical-border          nil :background 'unspecified)

;; Modeline
(set-face-attribute 'mode-line          nil :background subl-face-mode-line-bg)
(set-face-attribute 'mode-line-inactive nil :background subl-face-mode-line-inactive-bg)
(set-face-attribute 'mode-line-highlight nil :inherit nil :background 'unspecified :underline t)

;; Comint
(set-face-attribute 'comint-highlight-prompt nil :inherit nil)
;; (set-face-attribute 'ansi-color-blue    nil :foreground (face-foreground 'all-the-icons-blue))
;; (set-face-attribute 'ansi-color-cyan    nil :foreground (face-foreground 'all-the-icons-cyan))
;; (set-face-attribute 'ansi-color-red     nil :foreground (face-foreground 'all-the-icons-red))
;; (set-face-attribute 'ansi-color-green   nil :foreground (face-foreground 'all-the-icons-green))
;; (set-face-attribute 'ansi-color-magenta nil :foreground (face-foreground 'all-the-icons-dpink))

;; Helm
(set-face-attribute 'helm-selection              nil :inherit 'hl-line :background (face-background 'hl-line))
(set-face-attribute 'helm-selection-line         nil :inherit 'hl-line :background (face-background 'hl-line))
(set-face-attribute 'helm-match                  nil :foreground "#EBCB8B" :weight 'normal)
(set-face-attribute 'helm-match-item             nil :inherit 'region)
(set-face-attribute 'helm-grep-match             nil :inherit 'helm-match :background 'unspecified :foreground 'unspecified :weight 'unspecified)
(set-face-attribute 'helm-source-header          nil :weight 'bold :background 'unspecified)
(set-face-attribute 'helm-grep-lineno            nil :foreground 'unspecified :inherit 'line-number)
(set-face-attribute 'helm-ff-file                nil :foreground 'unspecified :background 'unspecified :weight 'normal)
(set-face-attribute 'helm-ff-file-extension      nil :foreground 'unspecified :background 'unspecified :weight 'normal)
(set-face-attribute 'helm-ff-symlink             nil :foreground 'unspecified :background 'unspecified :weight 'normal)
(set-face-attribute 'helm-ff-prefix              nil :foreground 'unspecified :background 'unspecified :weight 'normal)
(set-face-attribute 'helm-minibuffer-prompt      nil :background (face-background 'default))
(set-face-attribute 'helm-buffer-directory       nil :foreground (face-foreground 'all-the-icons-orange) :background 'unspecified)

;; Magit
(set-face-attribute 'magit-header-line                 nil :inherit 'helm-source-header :background 'unspecified :foreground 'unspecified :box 'unspecified)
(set-face-attribute 'magit-section-highlight           nil :background (face-background 'default))
(set-face-attribute 'magit-diff-file-heading-highlight nil :foreground (face-foreground 'all-the-icons-lblue) :weight 'bold :background 'unspecified)
(set-face-attribute 'magit-diff-file-heading           nil :foreground (face-foreground 'all-the-icons-lblue) :weight 'bold :background 'unspecified)
(set-face-attribute 'magit-diff-hunk-heading-highlight nil :foreground (face-foreground 'all-the-icons-lblue) :slant 'italic :weight 'normal :background 'unspecified)
(set-face-attribute 'magit-diff-hunk-heading           nil :foreground (face-foreground 'all-the-icons-lblue) :slant 'italic :weight 'normal :background 'unspecified)
(set-face-attribute 'magit-section-highlight           nil :foreground 'unspecified :background 'unspecified)
(set-face-attribute 'magit-diff-context                nil :foreground 'unspecified :background 'unspecified)
(set-face-attribute 'magit-diff-context-highlight      nil :foreground 'unspecified :background 'unspecified)
(set-face-attribute 'magit-diff-hunk-region            nil :inherit 'region)
(set-face-attribute 'magit-diff-added                  nil :foreground subl-face-diff-added-fg   :background (face-background 'default) :weight 'normal)
(set-face-attribute 'magit-diff-removed                nil :foreground subl-face-diff-removed-fg :background (face-background 'default) :weight 'normal)
(set-face-attribute 'magit-diff-added-highlight        nil :foreground subl-face-diff-added-fg   :background (face-background 'default) :weight 'normal)
(set-face-attribute 'magit-diff-removed-highlight      nil :foreground subl-face-diff-removed-fg :background (face-background 'default) :weight 'normal)

;; Company
(set-face-attribute 'company-tooltip                   nil :inherit nil
                                                           :foreground subl-face-default-fg
                                                           :background (doom-blend subl-face-default-bg "#ddeeff" 0.95) ':weight 'normal)
(set-face-attribute 'company-tooltip-selection         nil :foreground subl-face-default-fg
                                                           :background (doom-blend subl-face-default-bg "#ccddff" 0.90) ':weight 'normal)
(set-face-attribute 'company-tooltip-common-selection  nil :foreground (face-foreground 'helm-match) :weight 'normal :underline nil)
(set-face-attribute 'company-tooltip-common            nil :foreground (face-foreground 'helm-match) :weight 'normal :underline nil)
(set-face-attribute 'company-tooltip-scrollbar-track   nil :inherit nil
                                                           :foreground subl-face-default-fg
                                                           :background (doom-blend subl-face-default-bg "#ddeeff" 0.85) ':weight 'normal)

(set-face-attribute 'diff-refine-added                 nil :inverse-video nil :background subl-face-diff-refine-added-bg)
(set-face-attribute 'diff-refine-removed               nil :inverse-video nil :background subl-face-diff-refine-removed-bg)

;; Doom
(set-face-attribute 'doom-modeline-bar-inactive        nil :background (face-foreground 'mode-line-inactive))
(set-face-attribute 'doom-modeline-highlight           nil :inherit 'mode-line-highlight)

;; Flyspell
(custom-set-faces
 '(flyspell-duplicate ((t (:underline (:color "LightSteelBlue4" :style wave)))))
 '(flyspell-incorrect ((t (:underline (:color "LightSteelBlue4" :style wave))))
 '(font-lock-comment-face ((t (:foreground "LightSteelBlue4")))))
 '(font-lock-doc-face ((t (:inherit font-lock-comment-face :foreground "LightSteelBlue4")))))

;; Pdf-tools
(setq pdf-view-midnight-colors `(,(face-foreground 'default) . ,(face-background 'default)))

;; Tab mode
(set-face-attribute 'tab-bar-tab nil :foreground "gray" :background subl-face-default-bg :slant 'italic :weight 'bold)
(set-face-attribute 'tab-bar     nil :foreground subl-face-default-bg :background subl-face-default-bg)

;; Diff hl
(setq subl-face-diff-hl-change (face-background 'diff-hl-change))
(setq subl-face-diff-hl-delete (face-background 'diff-hl-delete))
(setq subl-face-diff-hl-insert (face-background 'diff-hl-insert))

(set-face-attribute 'diff-hl-change nil :foreground subl-face-diff-hl-change :background subl-face-default-bg :underline nil)
(set-face-attribute 'diff-hl-delete nil :foreground subl-face-diff-hl-delete :background subl-face-default-bg :underline nil)
(set-face-attribute 'diff-hl-insert nil :foreground subl-face-diff-hl-insert :background subl-face-default-bg :underline nil)

;; Show vertical line on truncated lines
(defface truncation-face '() "truncation-face")
(set-face-attribute 'truncation-face  nil :background nil :foreground subl-face-truncation-fg)

(set-display-table-slot standard-display-table 0
                        (make-glyph-code ?\x2503 'truncation-face))

;; Customize the "git-gutter"
(customize-set-variable 'diff-hl-margin-symbols-alist
  '((insert . "┃") (delete . "┃") (change . "┃")
    (unknown . "?") (ignored . "i")))
