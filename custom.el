;; Set font
(set-frame-font "Hack Nerd Font 12")
;; Set repo dir
(setq magit-repository-directories `(("~/repositories" . 5)))

;; Company specific code

;; -- Gitlab --

;; Set to "gitlab.yourcompany.com" to enable gitlab
(setq code-review-gitlab-base-url nil)

(if code-review-gitlab-base-url
    (progn
      (setq code-review-gitlab-host (concat code-review-gitlab-base-url "/api"))
      (setq code-review-gitlab-graphql-host code-review-gitlab-host)
      ;; -- Forge --
      (with-eval-after-load 'forge
        (add-to-list 'forge-alist
                     '(code-review-gitlab-base-url
                       (concat code-review-gitlab-host "/v4")
                       code-review-gitlab-base-url
                       forge-gitlab-repository)))))

